import * as React from "react";
import { BrowserRouter, Route, Link, Routes } from "react-router-dom";
import "./App.css";
import Header from "./components/header";
import People from "./page/People";
import { ChakraProvider } from "@chakra-ui/react";
import Planets from "./page/Planets";
import Films from "./page/Films";
function App() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" exact element={<Films />} />
        <Route path="/people" exact element={<People />} />
        <Route path="/planets" exact element={<Planets />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
