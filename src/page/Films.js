import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Button,
  Flex,
  Box,
  Grid,
  GridItem,
  Text,
  Input,
  Spinner,
} from "@chakra-ui/react";

function Films() {
  const [films, setFilms] = useState([]);
  const [sortOption, setSortOption] = useState(null);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [loading, setLoading] = useState(true); // State untuk menangani status loading

  const filteredFilms = selectedCategory
    ? films.filter((film) => film.category_id == selectedCategory)
    : films;

  const searchedFilms = searchTerm
    ? filteredFilms.filter(
        (film) =>
          film.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
          film.release_date.includes(searchTerm)
      )
    : filteredFilms;

  useEffect(() => {
    const fetchFilms = async () => {
      try {
        const res = await axios.get("https://swapi.dev/api/films/");
        setFilms(res.data.results);
      } finally {
        setLoading(false); // Setelah data diperoleh, atur status loading ke false
      }
    };

    fetchFilms();
  }, []);

  const sortedFilms = sortOption
    ? [...searchedFilms].sort((a, b) => {
        if (sortOption === "release_date_asc") {
          return a.release_date.localeCompare(b.release_date);
        } else if (sortOption === "release_date_desc") {
          return b.release_date.localeCompare(a.release_date);
        }
      })
    : searchedFilms;

  const handleCategoryFilter = (event) => {
    const categoryId = event.target.value;
    setSelectedCategory(categoryId);
  };

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
  };

  return (
    <Box mt={2}>
      <Box w="40%" alignItems="center" mb={2}>
        <Input
          type="text"
          placeholder="Search by title or release date"
          value={searchTerm}
          onChange={handleSearch}
          mr={2}
        />
        <Button onClick={() => setSortOption("release_date_asc")} mr={2}>
          Sort by Release Date (Asc)
        </Button>
        <Button onClick={() => setSortOption("release_date_desc")}>
          Sort by Release Date (Desc)
        </Button>
      </Box>
      {loading ? (
        <Flex justify="center" align="center" height="300px">
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Flex>
      ) : (
        <Grid templateColumns="repeat(3, 1fr)" gap={6}>
          {sortedFilms.map((film) => (
            <GridItem key={film.episode_id}>
              <Box borderWidth="1px" borderRadius="lg" p={4}>
                <Text fontWeight="bold">{film.title}</Text>
                <Text fontSize="sm" mt={2}>
                  Episode: {film.episode_id}
                </Text>
                <Text fontSize="sm" mt={2}>
                  Release Date: {film.release_date}
                </Text>
              </Box>
            </GridItem>
          ))}
        </Grid>
      )}
    </Box>
  );
}

export default Films;
