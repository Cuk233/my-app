import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Button,
  Flex,
  Box,
  Grid,
  GridItem,
  Text,
  Input,
  Spinner,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

function Planets() {
  const [planets, setPlanets] = useState([]);
  const [sortOption, setSortOption] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [loading, setLoading] = useState(true); // State untuk menangani status loading
  const itemsPerPage = 3;

  useEffect(() => {
    const fetchPlanets = async () => {
      try {
        const res = await axios.get(
          `https://swapi.dev/api/planets/?page=${currentPage}`
        );
        setPlanets(res.data.results);
      } finally {
        setLoading(false);
      }
    };
    setLoading(true); // Setelah currentPage berubah, atur status loading ke true
    fetchPlanets();
  }, [currentPage]);

  const searchedPlanets = searchTerm
    ? planets.filter(
        (planet) =>
          planet.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
          planet.population.includes(searchTerm)
      )
    : planets;

  const indexOfLastPlanet = currentPage * itemsPerPage;
  const indexOfFirstPlanet = indexOfLastPlanet - itemsPerPage;
  const currentPlanets = searchedPlanets.slice(
    indexOfFirstPlanet,
    indexOfLastPlanet
  );

  const sortedPlanets = sortOption
    ? [...currentPlanets].sort((a, b) => {
        if (sortOption === "name_asc") {
          return a.name.localeCompare(b.name);
        } else if (sortOption === "name_desc") {
          return b.name.localeCompare(a.name);
        }
      })
    : currentPlanets;

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
    setCurrentPage(1);
  };

  const handleSort = (option) => {
    setSortOption(option);
  };

  const navigate = useNavigate();

  const handlePageChange = (change) => {
    setCurrentPage((prevPage) => prevPage + change);
  };

  return (
    <Box mt={10}>
      <Flex alignItems="center" mb={2}>
        <Input
          type="text"
          placeholder="Search by name or population"
          value={searchTerm}
          onChange={handleSearch}
          mr={2}
        />
        <Button onClick={() => handleSort("name_asc")} mr={2}>
          Sort by Name (Asc)
        </Button>
        <Button onClick={() => handleSort("name_desc")} mr={2}>
          Sort by Name (Desc)
        </Button>
      </Flex>
      {loading ? (
        <Flex justify="center" align="center" height="300px">
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Flex>
      ) : (
        <>
          <Grid templateColumns="repeat(3, 1fr)" gap={6}>
            {sortedPlanets.map((planet) => (
              <GridItem key={planet.url}>
                <Box borderWidth="1px" borderRadius="lg" p={4}>
                  <Text fontWeight="bold">{planet.name}</Text>
                  <Text fontSize="sm" mt={2}>
                    Population: {planet.population}
                  </Text>
                </Box>
              </GridItem>
            ))}
          </Grid>
          <Flex mt={4} justify="center">
            <Button
              onClick={() => handlePageChange(-1)}
              mx={1}
              isDisabled={currentPage === 1}
            >
              Previous
            </Button>
            <Button onClick={() => handlePageChange(1)} mx={1}>
              Next
            </Button>
          </Flex>
        </>
      )}
    </Box>
  );
}

export default Planets;
