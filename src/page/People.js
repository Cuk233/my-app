import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Button,
  Flex,
  Box,
  Grid,
  GridItem,
  Text,
  Input,
  Spinner,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

function People() {
  const [people, setPeople] = useState([]);
  const [sortOption, setSortOption] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [loading, setLoading] = useState(true);
  const itemsPerPage = 3;

  useEffect(() => {
    const fetchPeople = async () => {
      try {
        const res = await axios.get(
          `https://swapi.dev/api/people/?page=${currentPage}`
        );
        setPeople(res.data.results);
      } finally {
        setLoading(false);
      }
    };
    fetchPeople();
  }, [currentPage]);

  const searchedPeople = searchTerm
    ? people.filter(
        (person) =>
          person.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
          person.birth_year.includes(searchTerm) ||
          person.gender.toLowerCase().includes(searchTerm.toLowerCase())
      )
    : people;

  const indexOfLastPerson = currentPage * itemsPerPage;
  const indexOfFirstPerson = indexOfLastPerson - itemsPerPage;
  const currentPeople = searchedPeople.slice(
    indexOfFirstPerson,
    indexOfLastPerson
  );

  const sortedPeople = sortOption
    ? [...currentPeople].sort((a, b) => {
        if (sortOption === "name_asc") {
          return a.name.localeCompare(b.name);
        } else if (sortOption === "name_desc") {
          return b.name.localeCompare(a.name);
        }
      })
    : currentPeople;

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
    setCurrentPage(1);
  };

  const handleSort = (option) => {
    setSortOption(option);
  };

  const navigate = useNavigate();

  const handlePageChange = (change) => {
    setCurrentPage((prevPage) => prevPage + change);
  };

  return (
    <Box mt={10}>
      <Flex alignItems="center" mb={2}>
        <Input
          type="text"
          placeholder="Search by name, birth year, or gender"
          value={searchTerm}
          onChange={handleSearch}
          mr={2}
        />
        <Button onClick={() => handleSort("name_asc")} mr={2}>
          Sort by Name (Asc)
        </Button>
        <Button onClick={() => handleSort("name_desc")} mr={2}>
          Sort by Name (Desc)
        </Button>
      </Flex>
      {loading ? (
        <Flex justify="center" align="center" height="300px">
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Flex>
      ) : (
        <>
          <Grid templateColumns="repeat(3, 1fr)" gap={6}>
            {sortedPeople.map((person) => (
              <GridItem key={person.url}>
                <Box borderWidth="1px" borderRadius="lg" p={4}>
                  <Text fontWeight="bold">{person.name}</Text>
                  <Text fontSize="sm" mt={2}>
                    Birth Year: {person.birth_year}
                  </Text>
                  <Text fontSize="sm" mt={2}>
                    Gender: {person.gender}
                  </Text>
                  {/* Add more information about the person as needed */}
                </Box>
              </GridItem>
            ))}
          </Grid>
          <Flex mt={4} justify="center">
            <Button
              onClick={() => handlePageChange(-1)}
              mx={1}
              isDisabled={currentPage === 1}
            >
              Previous
            </Button>
            <Button onClick={() => handlePageChange(1)} mx={1}>
              Next
            </Button>
          </Flex>
        </>
      )}
    </Box>
  );
}

export default People;
